import { HttpClient } from '@angular/common/http';
import { Component, Output } from '@angular/core';
import { PostResource } from './blog/services/PostResource';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PostResource],
})
export class AppComponent {
  private _http: HttpClient;
  private _postResource: PostResource;

  constructor(postResource: PostResource, http: HttpClient) {
    this._http = http;
    this._postResource = postResource;
  }

  @Output() public postList: any;
  @Output() public profileImg: any;
  @Output() public isEdited: boolean = false;
  @Output() public editedPostId: any;
  @Output() public editedPostDate: any;
  @Output() public textareaValue: any;

  ngOnInit(): void {
    //fetching the data from url
    this._http.get(this._postResource.URL).subscribe((data) => {
      console.log(data);
      this.converttoarray(data);
    });
  }
  //on edit click in post.component change button from post to edit
  // and add the content of post in textarea
  onEditClick(textareaEditedValue: string) {
    console.log(this.isEdited);
    this.isEdited = true;
    this.textareaValue = textareaEditedValue;
  }
  //take current id of the clicked post and transfer to the textaera.component
  transferId(id: number) {
    this.editedPostId = id;
    console.log(id);
  }
  //take current date of the clicked post and transfer to the textaera.component
  transferDate(date: number) {
    this.editedPostDate = date;
  }

  //sort data by date
  converttoarray(data: any) {
    this.postList = data.sort(
      (a: any, b: any) =>
        new Date(b.post_date).getTime() - new Date(a.post_date).getTime()
    );
    //take just the picture of user
    this.profileImg = this.postList[0].image_url;
  }
}
