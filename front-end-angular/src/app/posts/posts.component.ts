import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostResource } from '../blog/services/PostResource';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  providers: [PostResource],
})
export class PostsComponent implements OnInit {
  // data is stored in postList
  @Input() public postList: any;
  @Input() public isEdited: any;

  @Output() onEditClick = new EventEmitter();
  @Output() transferId = new EventEmitter();
  @Output() transferDate = new EventEmitter();

  //for dropdown meny to toggle on click the function is in post.component.html
  public dropdownIsOpen: any = {};

  // for using props in constructor()
  private _http: HttpClient;
  private _postResource: PostResource;

  constructor(postResource: PostResource, http: HttpClient) {
    this._http = http;
    this._postResource = postResource;
  }

  ngOnInit(): void {}

  //delete post function
  deletePost(id: number) {
    return this._http.delete(`${this._postResource.URL}/${id}`).subscribe(
      (result) => console.log(result),
      (err) => console.error(err)
    );
  }

  //edit post function
  editPost(i: number, id: number, date: string) {
    this.onEditClick.emit(this.postList[i].post_content);
    this.transferId.emit(id);
    this.transferDate.emit(date);
  }
}
