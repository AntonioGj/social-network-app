import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';


import { PostsComponent } from './posts.component';

@NgModule({
    imports: [
        CommonModule,
    
    ],
    exports: [PostsComponent],
    declarations: [PostsComponent],
    providers: [],
})
export class PostsModule { }
