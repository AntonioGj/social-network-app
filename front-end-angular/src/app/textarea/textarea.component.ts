import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { PostResource } from './../blog/services/PostResource';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css'],
  providers: [PostResource],
})
export class TextareaComponent implements OnInit {
  //Storing the value of textarea
  @Input() public textareaValue: any;
  @Input() public editedPostId: any;
  @Input() public editedPostDate: any;
  @Input() public profileImg: any;
  @Input() public postList: any;

  @Input() public isEdited: any;

  // on key up event to store value of textarea
  onKey(event: any) {
    this.textareaValue = event.target.value;
  }

  // for using props in constructor()
  private _http: HttpClient;
  private _postResource: PostResource;

  constructor(postResource: PostResource, http: HttpClient) {
    this._http = http;
    this._postResource = postResource;
  }

  //on click add new post
  public newPost(form: NgForm) {
    if (this.textareaValue) {
      this._http
        .post(this._postResource.URL, {
          user_name: 'Antonio',
          user_surname: 'Gjorgjievski',
          post_date: new Date().toString(),
          image_url:
            'https://i.pinimg.com/originals/17/56/8f/17568fcd478e0699067ca7b9a34c702f.png',
          post_content: this.textareaValue,
        })
        .subscribe((res) => {
          console.log(res);
        });
    }
    //after new post delete value of textarea
    this.textareaValue = '';
  }

  //on click update post
  public updatePost(form: NgForm) {
    if (this.textareaValue) {
      this._http
        .put(`${this._postResource.URL}/${this.editedPostId}`, {
          user_name: 'Antonio',
          user_surname: 'Gjorgjievski',
          post_date: this.editedPostDate,
          image_url:
            'https://i.pinimg.com/originals/17/56/8f/17568fcd478e0699067ca7b9a34c702f.png',
          post_content: this.textareaValue,
        })
        .subscribe((data) => {
          console.log(data);
        });
    }
    //after editing post delete value of textarea
    this.textareaValue = '';
  }

  ngOnInit(): void {}
}
