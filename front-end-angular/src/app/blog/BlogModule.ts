import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { PostsModule } from './../posts/postsModule';
import { PostResource } from './services/PostResource';


@NgModule({
    imports: [
        PostsModule,
        HttpClientModule,
    ],
    exports: [PostsModule],
    declarations: [],
    providers: [
        PostResource,

    ],
})
export class BlogModule { }
