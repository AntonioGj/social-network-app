import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiConfig } from './../../common/ApiConfig';

import { CreatePostDto } from './dataModel/CreatePostDto';


@Injectable()
export class PostResource {
    //take current url and adding to the url plus "posts" 
    public readonly URL = ApiConfig.url + "/posts";

    constructor(public http: HttpClient) {
        
    }
   
     public create(createPostDto: CreatePostDto){
         return this.http.post(this.URL, createPostDto) 
     }
  
}