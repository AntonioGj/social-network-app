import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogModule } from './blog/BlogModule';

@Module({
  imports: [
    BlogModule,
    //PostgreSQL
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '078901881',
      database: 'blog',
      entities: [__dirname + '/**/*Entity{.ts,.js}'],
      synchronize: true,
  }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
