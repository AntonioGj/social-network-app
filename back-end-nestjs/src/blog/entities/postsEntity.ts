import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class PostsEntity {
    @PrimaryGeneratedColumn()
    post_id: number;
    @Column({length: 500})
    user_name: string;
    @Column({length: 500})
    user_surname: string;
    @Column({length: 500})
    post_date: string;
    @Column("text")
    image_url: string;
    @Column("text")
    post_content: string;
}