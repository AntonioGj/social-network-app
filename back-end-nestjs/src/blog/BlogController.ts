import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { CreatePostDto } from './dtos/CreatePostDto';
import { PostsEntity } from './entities/postsEntity';
import { PostService } from './services/PostService';

@Controller('posts')
export class PostController {
  constructor(private postService: PostService) {}
  //Sending request over server to the posts resource
  @Get()
  findAll(): Observable<PostsEntity[]> {
    //accessing the post service
    return this.postService.findAll();
  }
  @Get(':id')
  async findOne(@Param('id') post_id: number) {
    // find the post with this id
    return await this.postService.findOneById(post_id);
  }

  @Delete(':id')
  remove(@Param('id') post_id: number) {
    return this.postService.remove(post_id);
  }

  @Post()
  create(@Body() createPostDto: CreatePostDto): Promise<CreatePostDto> {
    return this.postService.create(createPostDto);
  }

  @Put(':id')
  update(
    @Param('id') post_id: number,
    @Body() createPostDto: CreatePostDto,
  ): Promise<any> {
    createPostDto.post_id = Number(post_id);
    return this.postService.update(createPostDto);
  }
}
