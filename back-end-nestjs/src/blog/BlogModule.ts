import { Module } from "@nestjs/common";
import { PostController } from "./BlogController";
import { PostService } from "./services/PostService";
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostsEntity } from './entities/postsEntity';

@Module({
    imports: [
        TypeOrmModule.forFeature([PostsEntity]) ,  
    ],
    controllers: [
        PostController,
    ],
    providers: [
        PostService,
    ],
  })
  export class BlogModule {}
