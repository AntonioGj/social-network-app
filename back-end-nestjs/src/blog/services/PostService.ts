import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable, of } from 'rxjs';
import { Repository } from 'typeorm';
import { CreatePostDto } from '../dtos/CreatePostDto';
import { PostsEntity } from '../entities/postsEntity';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostsEntity)
    private readonly postRepository: Repository<PostsEntity>,
  ) {}
  //created message that will return the postEntity
  public findAll(): Observable<PostsEntity[]> {
    return from(this.postRepository.find());
  }

  public findOneById(post_id: any) {
    return this.postRepository.findOne({ where: { post_id } });
  }

  public remove(post_id: any) {
    return this.postRepository.delete(post_id);
  }

  public create(createPostDto: CreatePostDto): Promise<CreatePostDto> {
    return this.postRepository.save(createPostDto);
  }

  public update(createPostDto: CreatePostDto) {
    return this.postRepository.update(createPostDto.post_id, createPostDto);
  }
}
