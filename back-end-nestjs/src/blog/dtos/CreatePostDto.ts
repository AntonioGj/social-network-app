export class CreatePostDto {
    public post_id: number;
    public user_name: string;
    public user_surname: string;
    public post_date: string;
    public image_url: string;
    public post_content: string;
}